class ExporterServiceLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_formatted_s(:db)} #{severity} #{msg}\n"
  end
end

logfile = File.open("#{Rails.root}/log/exporter_services.log", 'a')  # create log file
logfile.sync = true  # automatically flushes data to file
EXPORTER_LOGGER = ExporterServiceLogger.new(logfile)  # constant accessible anywhere
