class AddTimeForCompletionToPreSurveys < ActiveRecord::Migration
  def change
    add_column :pre_surveys, :time_for_completion, :integer
  end
end
