class AddLocationColumnsToQuestionnaire < ActiveRecord::Migration
  def change
    add_column :pre_survey_socio_economic_characteristics, :home_location, :string
    add_column :pre_survey_socio_economic_characteristics, :work_locations, :string
    add_column :pre_survey_socio_economic_characteristics, :shop_locations, :string
    add_column :pre_survey_socio_economic_characteristics, :leisure_locations, :string
  end
end
