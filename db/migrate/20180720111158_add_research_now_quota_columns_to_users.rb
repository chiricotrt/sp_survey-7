class AddResearchNowQuotaColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :rn_age, :integer
    add_column :users, :rn_sex, :integer
    add_column :users, :rn_licence, :integer
  end
end
