class AddUserInformation < ActiveRecord::Migration
  def self.up
    add_column :users, :gender, :string
    add_column :users, :age, :string
    add_column :users, :education, :string
    add_column :users, :relationship, :string
  end

  def self.down
    remove_column :users, :gender
    remove_column :users, :age
    remove_column :users, :education
    remove_column :users, :relationship
  end
end
