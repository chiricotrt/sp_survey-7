class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :email
      t.string :crypted_password
      t.string :password_salt
      t.datetime :created_at
      t.datetime :updated_at
      t.references :household
      t.string :name
      t.references :role
      t.datetime :last_login_at
      t.datetime :current_login_at
      t.datetime :current_login_ip
      t.boolean :active

      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
