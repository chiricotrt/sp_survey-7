class ModifyAccessToVehicleFromBooleanToInteger < ActiveRecord::Migration
  def change
    remove_column :pre_survey_private_mobility_characteristics, :access_to_vehicle
    add_column :pre_survey_private_mobility_characteristics, :access_to_vehicle, :integer
  end
end
