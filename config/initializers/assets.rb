# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(common.js common.css pre-common.css vendor.js
  backofficeJS.js backofficeCSS.css hint.min.css shake.cs raven.min.js jquery.price_format.1.7.js
  surveyor_all.css surveyor_all.js timeline.css smart/canvasjs.min.js
  maas/sp_experiments.js maas/flexible_task.js maas/day_choices.js maas/new_home.js rails_bootstrap_forms.css
  bootstrap.css.map introjs.min.css multiple-select.css umove/attitudes.js umove/pre_surveys.js
  umove/resident_sp.js umove/statistics.js jquery.raty-fa.js sections/*.js sections.css)
