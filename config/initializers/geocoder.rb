Geocoder.configure(
	# geocoding service (see below for supported options):
  :lookup => :nominatim, # Default lookup provider is google. Other options: [:bing, :yandex, :nominatim, :mapquest]

  :timeout => 5,
  # set default units to kilometers:
  :units => :km,

  # Providers
	bing: {
		api_key: "Ag-60JWf4rIR9QL608nhS83Bz8Z2kPasfA358qQ8KUGrOp9FFfdXzq6a2mM3x6v4"
	},

	mapquest: {
		api_key: ""
	},

	# Use Redis as a cache (default prefix is 'geocoder')
	cache: $redis,

	http_headers: {
		"User-Agent" => "Singapore-MIT Alliance for Research and Technology <fmsurvey@smart.mit.edu>"
	}
)