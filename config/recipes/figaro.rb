namespace :figaro do
  desc "SCP transfer application.yml configuration file to the shared folder"
  task :setup, :roles => :app do
    transfer :up, "config/application.yml", "#{shared_path}/application.yml", via: :scp
  end

  desc "Symlink application.yml to the release path"
  task :symlink, :roles => :app do
    run "ln -sf #{shared_path}/application.yml #{release_path}/config/application.yml"
  end

  desc "Check if secrets.yml file exists on the server"
  task :check, :roles => :app do
    begin
        run "test -f #{shared_path}/application.yml"
      rescue Capistrano::CommandError
        unless fetch(:force, false)
          logger.important 'application.yml file does not exist on the server "shared/application.yml"'
          exit
        end
    end
  end


  set(:fm_happy_master,
    Capistrano::ServerDefinition.new('172.25.184.52', {
      user: 'deploy',
      ssh_options: {keys: "#{ENV['HOME']}/.ssh/deploy"}
    })
  )
  set(:fm_happy_slave,
    Capistrano::ServerDefinition.new('172.25.184.53', {
      user: 'deploy',
      ssh_options: {keys: "#{ENV['HOME']}/.ssh/deploy"}
    })
  )
  set(:fm_staging,
    Capistrano::ServerDefinition.new('172.25.184.19', {
      user: 'ubuntu',
      ssh_options: {keys: "#{ENV['HOME']}/.ssh/fms"}
    })
  )

  set(:fm_delhi_master) {
    Capistrano::ServerDefinition.new('54.254.167.234', {
      user: 'ubuntu',
      ssh_options: {keys: "#{ENV['HOME']}/.ssh/fms-delhi"}
    })
  }

  set(:fm_delhi_slave) {
    Capistrano::ServerDefinition.new('54.169.243.12', {
      user: 'ubuntu',
      ssh_options: {keys: "#{ENV['HOME']}/.ssh/fms-delhi"}
    })
  }

  set(:fm_boston_master) {
    Capistrano::ServerDefinition.new('54.173.61.115', {
      user: 'ubuntu',
      ssh_options: {keys: "#{ENV['HOME']}/.ssh/fms-boston"}
    })
  }
  set(:fm_boston_slave) {
    Capistrano::ServerDefinition.new('52.91.149.157', {
      user: 'ubuntu',
      ssh_options: {keys: "#{ENV['HOME']}/.ssh/fms-boston"}
    })
  }

  desc "Update mailer credentials for all servers using NUS mailbox"
  task :update_nus_mailer, hosts: [fm_staging] do
  # task :update_nus_mailer, hosts: [fm_happy_master, fm_happy_slave, fm_staging
  #   fm_delhi_master, fm_delhi_slave, fm_boston_master, fm_boston_slave
  # ] do
    set(:email_pwd) { Capistrano::CLI.password_prompt "New mailbox password: " }

    current_task.options[:hosts].each do |server|
      target = "/home/#{server.user}/#{application}/shared/application.yml"
      sed_command = "sed -i 's/EMAIL_PWD: \".*\"/EMAIL_PWD: \"#{email_pwd}\"/g' #{target}"

      run sed_command
    end
  end

  before "deploy", "figaro:check"
  after "deploy:setup", "figaro:setup"
  after "deploy:finalize_update", "figaro:symlink"
end

