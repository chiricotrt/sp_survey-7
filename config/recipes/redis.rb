set_default(:redis_pid) {"/var/run/redis_6379.pid"}
set_default(:redis_port, 6379)
set_default(:redis_version) {"redis-2.8.23"}


namespace :redis do
  desc "Install the latest release of Redis"
  task :install, roles: :app do
    run "#{sudo} apt-get install -y tcl8.5"
    run "cd /tmp && wget http://download.redis.io/releases/#{redis_version}.tar.gz"
    run "cd /tmp && tar xzf #{redis_version}.tar.gz"
    run "cd /tmp/#{redis_version}/ && make && #{sudo} make install"
    # echo skips all configurations, so everything is set to default (port, pid, log)
    run "cd /tmp/#{redis_version}/utils/ && echo | #{sudo} ./install_server.sh"
    run "rm -rf /tmp/#{redis_version}.tar.gz && rm -rf /tmp/#{redis_version}/"
  end
  after "deploy:install", "redis:install"

  %w[start stop restart].each do |command|
    desc "#{command} redis"
    task command, roles: :web do
      run "/etc/init.d redis_6379 #{command}"
    end
  end
end
