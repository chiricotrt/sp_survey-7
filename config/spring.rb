  Spring.after_fork do
    # This code will be run each time you run your specs.
    require "#{Rails.root}/test/shared_connection"
  end
