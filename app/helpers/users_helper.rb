module UsersHelper
  def study_title
    if params[:study].present?
      content_tag(:h4, "You are signing up for the #{params[:study]} study")
    end
  end

  def study_field
    if params[:study].present?
      hidden_field_tag('user[study]', params[:study])
    end
  end

  def create_user_url
    if params[:study].present?
      users_for_study_url(params[:study])
    else
      users_url
    end
  end
end
