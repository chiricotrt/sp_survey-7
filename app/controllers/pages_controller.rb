class PagesController < ApplicationController
  impressionist

	# filter_parameter_logging :curr_pass, :new_pass, :confirm_new_pass

	before_filter :require_user, :only => ['home', 'dashboard', 'consent', 'accept_consent',
    'feedback_activitydiary', 'calendar', 'index']

  layout 'smart'

  protect_from_forgery :except => [:calendar]

  # Loads the information sheet for the participant
  def info
  end

  # Redirects users to relevant pre-survey
  def gateway
    user_id = params[:user_id]

    begin
      city_id = City.city_from_country(params[:city_id] || params[:city])
    rescue
    end

    if city_id
      redirect_to sync_umove_users_path(umove_id: user_id, city_id: city_id), method: :post
    else
      render file: "#{Rails.root}/public/404", layout: false, status: :not_found
    end
  end

  def survey
    user_id = params[:user_id]
    begin
      city_id = City.city_from_country(params[:city_id] || params[:city])
    rescue
    end

    if city_id
      redirect_to sync_umove_users_path(umove_id: user_id, city_id: city_id, 
        user_email: params[:user_email],
        user_screen_responses: params[:user_screen],
        rn_id: params[:rn_id],
        rn_study: params[:rn_study], 
        lux_language: params[:lux_language]), method: :post
    else
      render file: "#{Rails.root}/public/404", layout: false, status: :not_found
    end
  end

  # RN: http://localhost:3000?city=manchester&rnid=TEST1&study=249428
  # RN: http://eu.maaspoll.com?city=manchester&rnid=TEST1&study=249428
  def new_home
    @hide_header = true
    @city = params[:city_id] || params[:city]
    @city_id = City.city_from_country(params[:city_id] || params[:city])
    @rn_id = params[:rnid]
    @rn_study = params[:study]

    @sex_options = [
      ["Male", 1],
      ["Female", 2],
      ["Prefer not to answer", 3]
    ]

    @binary_options = [
      ["Yes", 1],
      ["No", 0]
    ]

    @lux_language_options = [
      ["English", 1],
      ["Deutsch", 2],
      ["Français", 3]
    ]
  end

  def index
    flash.keep

    # if !current_user.nil? && !current_user.consent
    if not current_user.completedPresurvey
      # current_user.consent!
      redirect_to pages_home_path
    elsif params[:current_page].present?
      redirect_to params[:current_page]
    else
      redirect_to pages_home_path #:controller => "pages", :action => :home
    end
  end

  def thankyou
    @survey_progress = 100
  end

  def statistics
    response.headers.delete "X-Frame-Options"
    @hide_header = true
    @cities = City.all
  end

  def tab_1
    response.headers.delete "X-Frame-Options"
    render layout: "embed"
  end

  def tab_2
    response.headers.delete "X-Frame-Options"
    render layout: "embed"
  end

  def tab_3_1
    response.headers.delete "X-Frame-Options"
    render layout: "embed"
  end

  def tab_3_2
    response.headers.delete "X-Frame-Options"
    render layout: "embed"
  end

  def tab_3_3
    response.headers.delete "X-Frame-Options"
    render layout: "embed"
  end

  def summarise
    if params[:id] and params[:start_date] and params[:end_date]
      residents = Summariser.resident_total(params[:id], params[:start_date], params[:end_date])
      tourists = Summariser.tourist_total(params[:id], params[:start_date], params[:end_date])
      # residents = Summariser.resident_total(3, "2018-05-01", "2018-05-10")
      # tourists = Summariser.tourist_total(3, "2018-05-01", "2018-05-10")

      render json: { residents: residents, tourists: tourists }
    else
      render json: { status: false }
    end
    
  end

  def overview
    current_user.consent!
  end

  def image_test
  end

  def internal
    @token = params[:token]
    @user = User.new
  end

	def newuser
		@token = params[:token]
		@user = User.new
	end

  def home
    # Set up order of walk-through boxes
    index = 0
    @walk_through_counter = {}
    @walk_through_counter[:presurvey] = current_user.completedPresurvey ? -1 : (index += 1)
    # @walk_through_counter[:download_app] = current_user.downloaded_app? ? -1 : (index += 1)
    # @walk_through_counter[:activity_diary] = current_user.finished[0] ? -1 : (index += 1)
    # @walk_through_counter[:dashboard] = current_user.finished[0] ? -1 : (index += 1)
    # @walk_through_counter[:frequent_places] = current_user.finished[0] ? -1 : (index += 1)
    @walk_through_counter[:postsurvey] = (index += 1)
    @walk_through_counter[:contact] = (index += 1)
  end

	def home_mobile
		@promptWarning = current_user.promptWarning?
	end

	def home_mobile_gap
		@promptWarning = current_user.promptWarning?
	end

	def claim_incentive # deprecated?
		puts params.inspect

		#counters = current_user.getCounters
		counters = current_user.validatedDays(:counters)
		current_user.claim_incentive!(params, counters[1], counters[0])
		current_user.feedback_complete = "3"
		current_user.save

		flash[:feedback] = _("Thank you for completing the survey. You should receive your incentive within 4 weeks.")
		flash[:class] = "content_feedback"
		redirect_to :root
	end

  def support_submit
		surveytype = params[:surveyType]
		errormsg = params[:errormsg]
		steps = params[:steps]

		phonebrand = params[:phonebrand]
		androidversion = params[:androidversion]
		computerOS = params[:computerOS]
		browser = params[:browser]

		data = {:surveytype=>surveytype,
						:errormsg=>errormsg,
						:steps=>steps,
						:phonebrand=>phonebrand,
						:androidversion=>androidversion,
						:computerOS=>computerOS,
						:browser=>browser
					 }
    current_user.send_support_data!(data)
    current_user.send_support_notification!

    redirect_to root_path, notice: "Your message has been successfully sent to our team. We will do our best to respond to any questions or problems you may be having within 24 hours."
	end

  def deactivate_user
    current_user.update_attributes(last_login_at: nil, current_login_at: nil)
    current_user_session.try(:destroy)
    redirect_to root_path
  end

  def accept_consent
    current_user.consent = true
    current_user.save
    if (current_user.registration_source == 'web')
      redirect_to pre_questionnaire_url #attempt_presurvey_url
    else
      redirect_to root_url
    end
  end

  def dashboard
    flash[:notice] = nil

    current_user.ezlink_data = EzlinkData.new(user_id: current_user.id) unless !current_user.ezlink_data.nil?

    @days_validated, @days_collected = current_user.validatedDays(:counters)
    @points = current_user.points_from_data(@days_collected, @days_validated)
    @level = current_user.level(@points)

    @travel_info_hash = {}
    travel_info = current_user.stop.last_two_weeks.travel_info
    @travel_info_hash[:last_two_weeks] = Stop.format_travel_info(travel_info)

    travel_info = current_user.stop.all_days.travel_info
    @travel_info_hash[:all_days] = Stop.format_travel_info(travel_info)

    day_travel_info = current_user.stop.on_day.travel_info
    @travel_info_hash[:day] = Stop.format_travel_info(day_travel_info) unless day_travel_info.empty?
  end

  def calendar
    if request.post?
      puts params.inspect.to_s
      if (not params["slotId"].nil?)
        Slot.transaction do
          slot = Slot.find(params["slotId"], :lock => true)
          Booking.transaction do
            booking = Booking.where("slot_id = ? and user_id <> ?", params["slotId"], current_user.id).first
            if (not booking.nil?)
              flash[:error] = "Unfortunately the slot you selected is already taken."
            else
              booking = current_user.bookings.first_or_initialize
              booking.slot_id = params["slotId"].to_i
              if (not params["cancelBooking"].nil?)
                helper = User.find(booking.slot.user_id)
                Notifier.cancel_booking(booking.slot.starttime, booking.slot.endtime, booking.slot.user.email).deliver_now
                booking.destroy
                current_user.updatePromptProperty(STATE2)
              else
                if (not params["contactnumber"].nil? and not params["comment"].nil?)
                  booking.comment = params["comment"]
                  booking.contact = params["contactnumber"]
                  Notifier.confirm_booking(booking).deliver_now
                  booking.save
                  current_user.updatePromptProperty(STATE4)
                end
              end
            end
          end
        end
      end
      ActiveRecord::Base.connection.execute('UNLOCK TABLES')
    end

    starttime = DateTime.now.strftime("%Y-%m-%d %H:%M")


    slotsData = ActiveRecord::Base.connection.select_all("Select slots.id,date_format(starttime, \"%Y-%m-%d %H:%i\") as starttime, date_format(endtime, \"%Y-%m-%d %H:%i\") as endtime from slots left join bookings on slots.id = bookings.slot_id where starttime >= '#{starttime}' and (bookings.id IS NULL OR (bookings.id IS NOT NULL and bookings.user_id = #{current_user.id}))")
    slotsData.each do |row|
      row['starttime'] = DateTime.parse(row['starttime']).to_i
      row['endtime'] = DateTime.parse(row['endtime']).to_i
    end

    booking = ActiveRecord::Base.connection.select_all("Select id,slot_id from bookings where user_id = #{current_user.id}")
    @bookingsjson = booking.to_json

    @slotsjson = slotsData.to_json
    render
  end
end
