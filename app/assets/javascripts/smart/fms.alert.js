var smart;
if (smart === undefined) {
	smart = {};
}

smart.Alert = {};
smart.Alert.basic = function(type, msg, sticky){
	var alert_container = jQuery('.alert_container')
	if (alert_container.length == 0){
		console.log("div.alert_container does not exists!")
	}
	var new_alert = jQuery('<div class="ink-alert basic '+type+' backoffice_alert "><button class="ink-dismiss">&times;</button><p>'+msg+'</p></div>');
	jQuery('.alert_container').append(new_alert);

	jQuery('.ink-dismiss',new_alert).click(function(e){
		new_alert.remove();
	})

	if (sticky !== null && !sticky){
		setTimeout(function(){ new_alert.remove(); }, 5000);
	}
};

smart.Alert.Error = function(msg, sticky){
	smart.Alert.basic('error', msg, sticky);
};

smart.Alert.Info = function(msg, sticky){
	smart.Alert.basic('info', msg, sticky);
};

smart.Alert.Success = function(msg, sticky){
	smart.Alert.basic('success', msg, sticky);
};


smart.Alert.UserMisplacedHome = function(yesCallback, noCallback){
	var successCallback = function(){
		jQuery.unsubscribe("event:misplaced_home:yes");
		jQuery('#stop_overlay').hide();
		if (yesCallback !== undefined)
			yesCallback();
		else
			console.log("alertUserMisplacedHome yesCallback not defined");
	};
	var rejectCallback = function(){
		jQuery.unsubscribe("event:misplaced_home:no");
		jQuery('#stop_overlay').hide();
		if (noCallback !== undefined)
			noCallback();
		else
			console.log("alertUserMisplacedHome noCallback not defined");
	};

	jQuery.unsubscribe("event:misplaced_home:yes");
	jQuery.unsubscribe("event:misplaced_home:no");
	jQuery.subscribe("event:misplaced_home:yes", successCallback);
	jQuery.subscribe("event:misplaced_home:no", rejectCallback);

	jQuery('#stop_overlay .overlay_message').html(jQuery('#misplaced_home_msg_template').jqote());
	jQuery('#stop_overlay').show();
}

smart.Alert.UserOverTime = function(last_activity, new_main, yesCallback, noCallback){
	var successCallback = function(){
		jQuery.unsubscribe("event:user_over_time:yes");
		jQuery('#stop_overlay').hide();
		if (yesCallback !== undefined)
			yesCallback();
		else
			console.log("alertUserOverTime yesCallback not defined");
	};
	var rejectCallback = function(){
		jQuery.unsubscribe("event:user_over_time:no");
		jQuery('#stop_overlay').hide();
		if (noCallback !== undefined)
			noCallback();
		else
			console.log("alertUserOverTime noCallback not defined");
	};

	jQuery.unsubscribe("event:user_over_time:yes");
	jQuery.unsubscribe("event:user_over_time:no");
	jQuery.subscribe("event:user_over_time:yes", successCallback);
	jQuery.subscribe("event:user_over_time:no", rejectCallback);

	var time_result = getActivityDuration(last_activity);
	var tHours = Math.floor(time_result/60);
	var tMinutes = time_result-tHours*60;
	var timeStr = "";
	if (time_result > 60)
		timeStr += tHours+" hour"+((tHours==1)?"":"s");
	timeStr +=  (" " + tMinutes + " minute" + ((tMinutes==1)?"":"s"));

	console.log("ALERT USER OVER TIME: ", new_main);

	jQuery('#stop_overlay .overlay_message').html(jQuery('#over_time_msg_template').jqote({ activity_id: last_activity.index,
																							activity_name: smart.Diary._activitiesByImage[new_main].name,
																							time: timeStr
																						}));
	jQuery('#stop_overlay').show();
}

smart.Alert.UserSpeeding = function(yesCallback, noCallback){
	var successCallback = function(){
		jQuery.unsubscribe("event:speeding:no");
		jQuery('#stop_overlay').hide();
		if (yesCallback !== undefined){
			yesCallback();
		}
		else
			console.log("alertUserSpeeding yesCallback not defined");
	};
	var rejectCallback = function(){
		jQuery.unsubscribe("event:speeding:yes");
		jQuery('#stop_overlay').hide();
		if (noCallback !== undefined)
			noCallback();
		else
			console.log("alertUserSpeeding noCallback not defined");
	};

	jQuery.unsubscribe("event:speeding:yes");
	jQuery.unsubscribe("event:speeding:no");
	jQuery.subscribe("event:speeding:no", successCallback);
	jQuery.subscribe("event:speeding:yes", rejectCallback);

	jQuery('#stop_overlay .overlay_message').html(jQuery('#speeding_msg_template').jqote());
	jQuery('#stop_overlay').show();
}
