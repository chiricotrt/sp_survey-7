 // Initial Setup
 // -------------

 // Setting the namespace in case it wasn't set up before
var smart;
if (smart === undefined) {
	smart = {};
}

smart.Sequencer = function(){
	this.initialize();
};

smart.Sequencer.prototype = {
	
	initialize: function()
	{
		_.bindAll(this, "initialize", "registerStep", "registerAfterHook", "_iterateSteps", "_nextStep", "_finalStep");

		this._sequenceID = guid();//_s4() + "-" + _s4() + "-"+ _s4();
		this._sequenceStepEventName = "sequencer:iterate:" + this._sequenceID;
		console.log("Sequencer created with id: "+ this._sequenceID);
		this._afterCallback = null;
		this._steps = [];
	},

	registerStep: function(step){
		step.registerSequencer(this)
		this._steps.push(step);
	},

	run: function(){
		if (this._steps.length > 0){
			jQuery.subscribe(this._sequenceStepEventName, this._iterateSteps);
			jQuery.publish(this._sequenceStepEventName, [0, {}]);
		} else {
			console.log("Sequence: There is no registered Step");
		}
	},

	_iterateSteps: function(e, idx, options){
		console.log("_iterateSteps",arguments, idx);
		this._steps[idx].start(idx, options);
	},

	_nextStep: function(idx, options){
		if (idx+1 < this._steps.length){
			jQuery.publish(this._sequenceStepEventName, [idx+1, options]);
		} else {
			this._finalStep(options);
		}
	},

	_finalStep: function(options){
		console.log("finalStep",options);
		jQuery.unsubscribe(this._sequenceStepEventName, this._iterateSteps);
		if (this._afterCallback !== null){
			this._afterCallback(options);
		}
	},

	registerAfterHook: function(_callback){
		this._afterCallback = _callback;
	}
};

smart.SequenceStep = function(initStep, cleanStep){
	if (!initStep || !cleanStep)
		throw new Error("SequenceStep needs to have an init and clean step callbacks defined!");

	this.initialize(initStep, cleanStep);
}

smart.SequenceStep.prototype = {

	initialize: function(initStep, cleanStep){
		_.bindAll(this, "registerSequencer", "start", "end", "initStep", "cleanStep");
		this._stepId = "step:end:"+guid();
		this._initStepCallback = initStep;
		this._cleanStepCallback = cleanStep;
	},

	registerSequencer: function(sequencer){
		this._parentSequencer = sequencer;
	},

	start: function(index, options){
		this.currentIdx = index;
		this._output = options;
		this.initStep();
	},

	end: function(e, output){
		this._output = _.extend(this._output, output);
		this.cleanStep();
		this._parentSequencer._nextStep(this.currentIdx, this._output);
	},

	initStep: function(){
		console.log("Initiating step "+ this.currentIdx);
		jQuery.subscribe(this._stepId, this.end);
		this._initStepCallback(this);
	},

	cleanStep: function(){
		console.log("Cleaning step "+ this.currentIdx);
		jQuery.unsubscribe(this._stepId, this.end);
		this._cleanStepCallback(this)
	}
}

/*var demo = new smart.Sequencer();
demo.registerStep(new smart.SequenceStep(function(step){ console.log("step1"); jQuery.publish(step._stepId,[{step1: true}]); }, function(){} ));
demo.registerStep(new smart.SequenceStep(function(step){ console.log("step2"); jQuery.publish(step._stepId,[{step2: true}]); }, function(){}));
demo.run();*/
