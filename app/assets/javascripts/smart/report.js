var alertFallback = true;
if (typeof console === "undefined" || typeof console.log === "undefined") {
	console = {};
	if (alertFallback) {
		console.log = function(msg) { alert(msg);};
	} else {
		console.log = function() {};
	}
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function showTracesButton() {
	jQuery("#traces_controller").css('display', '');
}

function hideTracesButton(){
	jQuery("#traces_controller").css('display', 'none');
	jQuery("#map_legend p").css('display', 'none');
}
function updateTraces(){
	console.log("calling show traces: "+cb_map_control);
	cb_map_control = !cb_map_control;
	var lines = smart.Map._traces;

	if (lines != null){
		for(var i=0; i < lines.length; i++){
			if (cb_map_control){
				lines[i].setMap(smart.Map._map);
			}
			else{
				lines[i].setMap(null);
			}
		}
	}

	jQuery("#map_legend").css('display', '');
	if (cb_map_control){
		jQuery("#trace_control").text('Hide Traces');
		jQuery("#map_legend p").css('display', '');
	}
	else{
		jQuery("#trace_control").text('Show Traces');
		jQuery("#map_legend p").css('display', 'none');
	}
}

function updateSubtitle(id, text){
	jQuery('#'+id).removeClass();
	jQuery('#'+id).addClass('legenda');
	jQuery('#'+id).text(text);
}

function validateOverTime(ident, tableid, temp_str, curr_activities, new_main_name){
	alertUserOverTime(curr_activities, new_main_name,
			function(){ // accept
				console.log("updateSingleIcon: "+ident+ " ||| "+curr_activities["mainactivity"]);

				if (curr_activities["mainactivity"] == "" || ident.indexOf(curr_activities["mainactivity"]) == -1){
					updateMapMarker(ident, curr_activities["index"]);
				}
				else{
					updateMapMarker("", curr_activities["index"]);
				}

				updateActivityModeIcon(ident, tableid);
				activateSaveButton(temp_str[1]);
				saveStopInfo(curr_activities.index, true);
			},
			function(){  // reject
				console.log('UPDATING SINGLE ICON MAIN ACTIVITY: user is going to change something');
				return;
			}
		);
}

function resetMapMarker(id){
	marker_index = markersId2Idx[whichAccordionIsOpen()];
	var shortPath = markers[marker_index].getIcon().split("/");
	var imageName = shortPath[3].split(".");
	new_marker_icon = "/"+shortPath[1]+"/"+shortPath[2]+"/activity-default."+imageName[1];

	markers[marker_index].setIcon(new_marker_icon);

	iconsBackup[marker_index] = new_marker_icon;
}

function validateForm(){
	var missing_info =false;
	var stop_real_id = whichAccordionIsOpen();

	if (isRowTravel()) {
		var curr_travel = getTravelData(stop_real_id);
		if (stop_real_id != first_stop_id){

			if (isRowTravel() && curr_travel["mode"] == ""){
				console.log("choose a mode - " + validating_id);
				//update subtitle of mode to error
				jQuery("#mode_sub-"+stop_real_id).removeClass();
				jQuery("#mode_sub-"+stop_real_id).addClass('activity_error');
				jQuery("#mode_sub-"+stop_real_id).text("Choose a mode");
				missing_info = true;
				triedClosing = true;
			}
		}
	} else {
		var curr_activities = getActivityData(stop_real_id);
		if(curr_activities["activities"].length < 1){
			console.log("choose an activity - " + stop_real_id);
			//update subtitle of activities to error
			jQuery("#activity_sub-"+stop_real_id).removeClass();
			jQuery("#activity_sub-"+stop_real_id).addClass('activity_error');
			jQuery("#activity_sub-"+stop_real_id).text("Choose an activity");
			missing_info = true;
			triedClosing = true;

		}else{
			if (curr_activities["mainactivity"] == ""){
				//update subtilte of main activity to error
				jQuery("#main_activity_sub-"+stop_real_id).removeClass();
				jQuery("#main_activity_sub-"+stop_real_id).addClass('activity_error');
				jQuery("#main_activity_sub-"+stop_real_id).text("Choose a main activity");
				missing_info = true;
				triedClosing = true;
			}
		}
	}

	return !missing_info;
}

/**
* Returns the zoom level at which the given rectangular region fits in the map view.
* The zoom level is computed for the currently selected map type.
* @param {google.maps.Map} map
* @param {google.maps.LatLngBounds} bounds
* @return {Number} zoom level
**/
function getZoomByBounds( map, bounds ){
  var MAX_ZOOM = map.mapTypes.get( map.getMapTypeId() ).maxZoom || 21 ;
  var MIN_ZOOM = map.mapTypes.get( map.getMapTypeId() ).minZoom || 0 ;

  var ne= map.getProjection().fromLatLngToPoint( bounds.getNorthEast() );
  var sw= map.getProjection().fromLatLngToPoint( bounds.getSouthWest() );

  var worldCoordWidth = Math.abs(ne.x-sw.x);
  var worldCoordHeight = Math.abs(ne.y-sw.y);

  //Fit padding in pixels
  var FIT_PAD = 40;

  for( var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom ){
      if( worldCoordWidth*(1<<zoom)+2*FIT_PAD < jQuery(map.getDiv()).width() &&
          worldCoordHeight*(1<<zoom)+2*FIT_PAD < jQuery(map.getDiv()).height() )
          return zoom;
  }
  return 0;
}

function centerInTravel(marker_id){
	var pos = markersId2Idx[marker_id];
	//console.log('marker position ('+marker_id+')is ',pos)
	setMarkersZIndex([pos, pos-1], 999, 0);

	var tempBounds = new google.maps.LatLngBounds();
	if (pos - 1 < 0)
		temp_pos = 1;
	else
		temp_pos = pos;

	for(var i=temp_pos-1; i <= temp_pos; i++){
		if (markers != null && markers[i] != null) {
			tempBounds.extend(markers[i].getPosition());
		}
	}
	map.fitBounds(tempBounds);
}

function centerInPoint(marker_id){
	console.log(markersId2Idx);
	var pos = markersId2Idx[marker_id];
	console.log('marker position ('+marker_id+')is ',pos);

	setMarkersZIndex([pos+1, pos, pos-1]);

	var tempBounds = new google.maps.LatLngBounds();
	temp_pos = pos;

	for(var i=temp_pos-1; i <= temp_pos+1; i++){
		if (markers != null && markers[i] != null) {
			tempBounds.extend(markers[i].getPosition());
		}
	}

	var newZoom = getZoomByBounds(map, tempBounds);
	// console.log("new zoom = ",newZoom);
	if (newZoom < 14) {
		map.setCenter(markers[pos].getPosition(), 14);
	} else {
		map.fitBounds(tempBounds);
	}

}

function updateMarkers(pos){
	for(var i=pos; i<markers.length ; i++){
		if(markers[i] != null){
			markers[i].labelContent = ""+(parseInt(markers[i].labelContent,10) + 1);
			markers[i].setMap(null);
			markers[i].setMap(map);
		}
	}
}

function getIconTransp(imagePath) {
	var shortPath = imagePath.split("/");
	var imageName = shortPath[3].split(".");
	if (imageName[0].indexOf("transp") > -1)
		final_name = "/"+shortPath[1]+"/"+shortPath[2]+"/"+imageName[0]+"."+imageName[1];
	else
		final_name = "/"+shortPath[1]+"/"+shortPath[2]+"/"+imageName[0]+"_transp."+imageName[1];
	//console.log(final_name);
	return final_name;
}

function focusInMap(id, options){
	if (isRowTravel(id)){
		var result = id.match(/[\d]+/);
		if (result !== null){
			result = result[0];
		}
		focusTravel(parseInt(result, 10), options);
	} else {
		focusActivity(parseInt(id, 10), options);
	}
}

function focusTravel(id, options){
	console.log("focus in travel: ",id);
	if (!isNaN(id)){
		var tmpMarkers = [markersId2Idx[id]];
		putMapMarkersTranspExcept( [markersId2Idx[id], markersId2Idx[id]-1 ] );
		centerInTravel(id);
	}
	else{
		putMapMarkersBack();
		showDaySummary();
		//jQuery("#addressTolatlon").hide();
	}
}

function focusActivity(id, options){
	console.log("focus in activity: ",id);
	putMapMarkersTranspExcept([markersId2Idx[id]]);
	centerInPoint(id);
}

function showDaySummary(){
	var tempBounds = new google.maps.LatLngBounds();
	for(var i=0; i <= markers.length; i++){
		if (markers != null && markers[i] != null) {
			tempBounds.extend(markers[i].getPosition());
		}
	}
	map.fitBounds(tempBounds);
}

function openNextAccordion(i){
	var isNextTravel = false;
	isNextTravel = !isRowTravel(i);

	var nextId = jQuery('input[name=\"stop_real_id\"]',jQuery('#'+(""+i).toLowerCase()+' + form')).val();
	if (nextId != undefined) {
		nextElm = (isNextTravel?'Travel':'')+nextId;
		checkAccompanying(nextElm);
		loadData(nextElm);
		runAccordion(nextElm);
		focusInMap(nextElm);
	} else {
		runAccordion(i);
		focusInMap("Travelundefined");
	}
}

function getNextStopId(i){
	var nextId = jQuery('input[name=\"stop_real_id\"]',jQuery('#'+(""+i).toLowerCase()+' + form')).val();
	return nextId;
}
function getPreviousStopId(i){
	var prevId = jQuery('input[name=\"stop_real_id\"]',jQuery('#'+("travel"+i).toLowerCase()).prev()).val();
	return prevId;
}

function onRowClose(event, row_id, isTravel){
	if (isTravel){
	} else {

	}
	smart.NearbyLocations.unload();
}

function onRowOpen(event, row_id, isTravel){
	console.log(row_id);
	smart.NearbyLocations.unload();
	if (isTravel){
		console.log("You opened an travel row");
	} else {
		console.log("You opened an Activity row");
		smart.NearbyLocations.load(row_id);
	}
}


function expandAccordion(i, counter){
	try {
		if( !hasAccordionOpen() ){
			accordion_opened = true;
			//console.log('acordian not open ',i)
			runAccordion(i);
			validating_id=whichAccordionIsOpen();
			loadData(i);

			focusInMap(i);
		}else{
			if(!stop_changed){
				runAccord = true;
				//if stop has not been changed and it's still not validated
				class_names = jQuery('#'+validating_id).find('#stop_number_'+validating_id).attr('class');
				//submit for validation
				if(class_names != undefined && class_names.indexOf('validated') == -1){
					runAccord = submitForm();
				}
				if(runAccord || validating_id != i){
					runAccordion(i);
					validating_id=whichAccordionIsOpen();
					loadData(i);

					focusInMap(i);
				}
			}
			else{
				if (submitForm()){
					runAccordion(i);
					validating_id=whichAccordionIsOpen();
					loadData(i);
					focusInMap(i);
				}
			}
		}
		//centerMap(i);
		changeMapLocation(markersId2Idx[i]);

		if (whichAccordionIsOpen() === null){
			putMapMarkersBack();

			//jQuery('#addressTolatlon').hide();
			jQuery("#addressTolatlon input").each(function(){
				jQuery(this).attr('disabled', 'disabled');
			});
			jQuery("#addressTolatlon").addClass("disabled");
		}
		else{
			jQuery('#addressTolatlon').show();
			jQuery("#addressTolatlon").removeClass("disabled");
			jQuery("#addressTolatlon input").each(function(){
				jQuery(this).removeAttr('disabled');
			});
		}

	} catch(e) {
		DEBUG.logException(e, 'expandAccordion');
	}
}

function putMapMarkersBack(){
	for(i=0;i<markers.length;i++){
		if (markers[i].icon.indexOf("_transp") != -1 ){
			parts = markers[i].icon.split("_transp");
			markers[i].setIcon(parts[0]+parts[1]);
		}
		markers[i].setZIndex(1);
	}
}

function putMapMarkersTranspExcept(not_transp){
	for(i=0;i<markers.length;i++){
		if (markers[i].icon.indexOf("_transp") != -1 ){
			parts = markers[i].icon.split("_transp");
			markers[i].setIcon(parts[0]+parts[1]);
		}
		if (not_transp.indexOf(i) == -1){
			markers[i].setIcon(getIconTransp(markers[i].icon));
			markers[i].setZIndex(1);
		} else {
			markers[i].setZIndex(9999);
		}
	}
}

function clone(obj) {
	if (null == obj || "object" != typeof obj) return obj;
	var copy = obj.constructor();
	for (var attr in obj) {
		if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
	}
	return copy;
}


function activateSaveButton(id){
	//console.log("activate svae button  "+id)
	var elm = jQuery('#'+id+' .AccordionContent .save_button');
	elm.removeClass('disabled');
	//elm.html('Save');
	updateAccordion(id);
}


function updateTime(evt, stop_id, parsed_starttime, parsed_endtime){
	try {
		console.log(stop_id, parsed_starttime, parsed_endtime);
		jQuery('#travel'+stop_id+' .end_time_header .timevalue').html(parsed_starttime);
		jQuery('#'+stop_id+' .start_time_header .timevalue').html(parsed_starttime);
		jQuery('#'+stop_id+' .end_time_header .timevalue').html(parsed_endtime);
		jQuery('.start_time_header .timevalue', jQuery('#'+stop_id+' + form')).html(parsed_endtime);
		jQuery('#'+stop_id+' .change_time_button').hide();
		jQuery('#'+stop_id+' .timechange_warnings').html("");
	} catch(exp){
		DEBUG.logException(exp, "updateTime");
	}
}

function updateTimeAndValidate(e, stop_id, parsed_starttime, parsed_endtime){
	updateTime(e, stop_id, parsed_starttime, parsed_endtime);
	smart.messageBox.validateActivities();
}

function updateStopHiddenInfo(evt, stopid, lat, lng){
	jQuery("#"+stopid+" input[name=stop_lat]").val(lat);
	jQuery("#"+stopid+" input[name=stop_lng]").val(lng);
}

//wherever we want to invoke foursquare api call this:
//smart.externalServices.runService('foursquare_venues', ["1.3667","103.75"], function(data){console.log("callback:"+data)});

//calculates distance between two points in km's
function calcDistance(p1, p2){
  return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
}

function showWeMissYourVoice(e, dialogname){
	if (dialogname != ''){
		jQuery( "#"+dialogname).show();
		jQuery('#'+dialogname +' .xClose').on('click', function(){submitCancelation(dialogname);});
	}
}

jQuery.subscribe('weMissYourVoice', showWeMissYourVoice);

function submitCancelation(dialogname){
	//alert(dialogname);
	jQuery.ajax({
		type: 'POST',
		url: '/users/submitIgnorePopup',
		data: { dialogname: dialogname },
		error: function(err){
			console.log(err);
		}
	});
}

function validateSync(result){
	if (result['synched']==false){
		jQuery('#stop_overlay .overlay_message').html(jQuery('#out_of_sync_msg').jqote());
		jQuery('#stop_overlay').show();
	}
}

// result needs to have keys: redraw, day
function redrawMap(result){
	// console.log(result);
	if (result["redraw"] === true){
		//get updated stops again
		showWaitIndicator();
		jQuery.ajax({
			url: "/report/get_all_stops",
			type: 'GET',
			data: { "day": result["day"] },
			success: function(new_result) {
				var old_stop_count = smart.Diary._currentDay["stops"].length;
				var new_stop_count = new_result["stops"].length;
				// console.log("old and new counts", old_stop_count, new_stop_count);

				if ((new_stop_count - old_stop_count) > 1) {
					var message_hash = {
						"message" : {
							"text" : "New stops have been loaded for this day.",
							"class" : "content_feedback"
						}
					};
					smart.Diary.showMessages(message_hash);
				}

				smart.Diary._currentDay = new_result;
				hideWaitIndicator();
				jQuery.publish("activities-fetched");

				if (undefined !== result["stop"]) {
					var stop_contents = smart.Diary.getStopRowFromStopId(result["stop"].stop.id);
					var current_idx = stop_contents["row"];
					smart.Diary.toggleAccordion(current_idx, 'activity');
				}
			}
		});
	}
	else{
		hideWaitIndicator();
		// console.log("redraw not needed -or- ERROR HAPPENED ADDING STOP");
	}
}


function getActivityDuration(stopData){
	var duration = smart.utils.getMomentDuration(stopData['start_time'], stopData['end_time']);
	return duration.asMinutes();
}

function timeConsistentHITS(stopData, new_main){

	if (stopData['mainactivity']===new_main){
		return -1;
	}

	var duration = smart.utils.getMomentDuration(stopData['start_time'], stopData['end_time']);
	time_dif = duration.asMinutes();
	console.log(time_dif);

	if(new_main !== ''){
		min_time_compare = _activitiesByImage[new_main]['min_time'];
		max_time_compare = _activitiesByImage[new_main]['max_time'];

		if (min_time_compare === ''){
			min_time = -1;
		}

		if (max_time_compare === ''){
			max_time_compare = 10000;
		}

		if (time_dif < min_time_compare || time_dif > max_time_compare){
			return time_dif;
		}
	}

	return -1;
}

function parseCalendarAnswer(answer){
	console.log(answer);
	if ( answer!= undefined && answer.error === 402){
		jQuery("#messageBoxWrapper").hide();
		jQuery(".notice_end").show();
		jQuery(".second_block").hide();
		jQuery("#stoppage_table").html(jQuery('#template_not_good_day_validate').jqote());
	}
}

function _s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
};

function guid() {
  return _s4() + _s4() + '-' + _s4() + '-' + _s4() + '-' +
         _s4() + '-' + _s4() + _s4() + _s4();
}


var timePickerOptions = {
	onClose: function(time, inst){
		var ptime = processTime(time);
		var instance_parts = inst.id.split('_');
		if(time_changed){

			var dataObj = {};
			if (instance_parts[0] == 'start'){
				//console.log("start--" , instance_parts);
				var time2 = jQuery('#end_time_'+instance_parts[2]).val();

				dataObj.starttime = time;
				dataObj.time_id = inst.id;

				if (time2!=undefined){
						dataObj.endtime = time2;
				}

			}else if (instance_parts[0] == 'end'){
				//console.log(instance_parts);
				var time2 = jQuery('#start_time_'+instance_parts[2]).val();

				dataObj.endtime = time;
				dataObj.time_id = inst.id;

				if (time2!=undefined){
						dataObj.starttime = time2;
				}
			}

			DEBUG.logMessage("Please change eval() use in report/_stoppageTable.html:181");
			jQuery.ajax({
					url: "<%= url_for :action=>'updateTime' -%>",
					data: dataObj,
			}).success(function(data){
					eval(data);
			});

			time_changed = false;
			stop_changed = false;
		} else {
			activateSaveButton(instance_parts[2])
			smart.messageBox.validateActivities();
		}
	}
}
