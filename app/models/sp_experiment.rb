require 'exporter_service_logger'

class SpExperiment < ActiveRecord::Base
  attr_accessible :user_id, :completed, :plans, :responses, :flexible_plan, :attitudes, :completion_times, :locale

  serialize :plans, JSON
  serialize :responses, JSON
  serialize :flexible_plan, JSON
  serialize :attitudes, JSON
  serialize :completion_times, JSON

  belongs_to :user

  before_save :choose_plans

  def choose_plans
    self.plans = {} if plans.nil?
    return if plans.present?

    # set plan variables
  end

  ## CONSTANTS ##
  DOWNLOAD_MAAS_APP_OPTIONS = {
    "resident" => [
      [:reason_1, 1],
      [:reason_2, 2],
      [:reason_3, 3],
      [:reason_4, 4],
      [:reason_5, 5]
    ],

    "tourist" => [
      [:reason_1, 1],
      [:reason_2, 2],
      [:reason_3, 3],
      [:reason_4, 4]
    ]
  }

  REASONS_FOR_NOT_DOWNLOADING_OPTIONS = {
    "resident" => [
      [:reason_1, 1],
      [:reason_2, 2],
      [:reason_3, 3],
      [:reason_4, 4],
      [:reason_5, 5]
    ],

    "tourist" => [
      [:reason_1, 1],
      [:reason_2, 2],
      [:reason_3, 3],
      [:reason_4, 4],
      [:reason_5, 5],
      [:reason_6, 6]
    ]
  }

  IMPORTANCE_OF_FEATURES = {
    "resident" => {
      "journey_planning" => [
        [:importance_of_features_journey_planning_1, "pfeat_jp_1"],
        [:importance_of_features_journey_planning_2, "pfeat_jp_2"],
        [:importance_of_features_journey_planning_3, "pfeat_jp_3"],
        # [:importance_of_features_journey_planning_4, "pfeat_jp_4"],
        [:importance_of_features_journey_planning_5, "pfeat_jp_5"],
        # [:importance_of_features_journey_planning_6, "pfeat_jp_6"],
        # [:importance_of_features_journey_planning_7, "pfeat_jp_7"],
        [:importance_of_features_journey_planning_8, "pfeat_jp_8"],
        [:importance_of_features_journey_planning_9, "pfeat_jp_9"]
      ],

      # "payment_ticketing" => [
      #   [:importance_of_features_payment_ticketing_1, "pfeat_pm_1"],
      #   [:importance_of_features_payment_ticketing_2, "pfeat_pm_2"],
      #   [:importance_of_features_payment_ticketing_3, "pfeat_pm_3"]
      # ],

      "other" => [
        [:importance_of_features_other_1, "pfeat_oth_1"],
        [:importance_of_features_other_2, "pfeat_oth_2"],
        # [:importance_of_features_other_3, "pfeat_oth_3"],
        [:importance_of_features_other_4, "pfeat_oth_4"],
        [:importance_of_features_other_5, "pfeat_oth_5"],
        [:importance_of_features_other_6, "pfeat_oth_6"],
        [:importance_of_features_other_7, "pfeat_oth_7"]
      ]
    },

    "tourist" => [
      [:importance_of_features_1, 1],
      [:importance_of_features_2, 2],
      [:importance_of_features_3, 3],
      [:importance_of_features_4, 4],
      [:importance_of_features_6, 5],
      [:importance_of_features_16, 6],
      [:importance_of_features_17, 7],
      [:importance_of_features_9, 8]
      # [:importance_of_features_12, "tmaasfeature9"]
    ]
  }

  REASONS_FOR_NOT_CHOOSING_OPTIONS = {
    "tourist" => [
      [:option_1, 1],
      [:option_2, 2],
      [:option_3, 3],
      [:option_4, 4],
      [:option_5, 5],
      [:option_6, 6],
      [:option_8, 8]
    ],

    "resident" => [
      [:option_1, 1],
      [:option_2, 2],
      [:option_3, 3],
      [:option_4, 4],
      [:option_5, 5],
      [:option_6, 6],
      [:option_7, 7],
      [:option_8, 8]
    ]
  }

  ADDONS = {
    "group_1": [
      { "id": 1, "label": :addon_1 },
      { "id": 2, "label": :addon_2 },
      { "id": 3, "label": :addon_3 }
    ],

    "group_2": [
      { "id": 4, "label": :addon_4 },
      { "id": 5, "label": :addon_5 }
    ],

    "group_3": [
      { "id": 6, "label": :addon_6 },
      { "id": 7, "label": :addon_7 },
      { "id": 8, "label": :addon_8 }
    ],
  }

  # MODE_FEATURES = {
  #   "1" => { price: 60.50, group: "Public Transport", icon: "public_transport", label: "Unlimited bus", id: 1, value: 1 },
  #   "2" => { price: 83.75, group: "Public Transport", icon: "public_transport", label: "Unlimited bus, tram, metro", id: 2, value: 2 },
  #   "3" => { price: 44.00, group: "Public Transport", icon: "public_transport", label: "10 daily tickets for all modes", id: 3, value: 3 },
  #   "4" => { price: 5.00, group: "Bike-sharing", icon: "bike", label: "5 hours", id: 4, value: 5 },
  #   "5" => { price: 10.00, group: "Bike-sharing", icon: "bike", label: "10 hours", id: 5, value: 10 },
  #   "6" => { price: 5.00, group: "Bike-sharing", icon: "bike", label: "Unlimited", id: 6, value: 99 },
  #   "7" => { price: 40.00, group: "Taxi", icon: "taxi", label: "2 trips", id: 7, value: 1 },
  #   "8" => { price: 70.00, group: "Taxi", icon: "taxi", label: "4 trips", id: 8, value: 2 },
  #   "9" => { price: 140.00, group: "Taxi", icon: "taxi", label: "8 trips", id: 9, value: 3 },
  #   "10" => { price: 220.00, group: "Taxi", icon: "taxi", label: "14 trips", id: 10, value: 4 },
  #   "11" => { price: 350.00, group: "Taxi", icon: "taxi", label: "Unlimited", id: 11, value: 99 },
  #   "12" => { price: 44.70, group: "Car-sharing", icon: "car_sharing", label: "5 hours", id: 12, value: 5 },
  #   "13" => { price: 65.40, group: "Car-sharing", icon: "car_sharing", label: "10 hours", id: 13, value: 10 },
  #   "14" => { price: 86.10, group: "Car-sharing", icon: "car_sharing", label: "15 hours", id: 14, value: 15 },
  #   "15" => { price: 120.00, group: "Car-sharing", icon: "car_sharing", label: "20 hours", id: 15, value: 20 },
  #   "16" => { price: 180.0, group: "Car-sharing", icon: "car_sharing", label: "Unlimited", id: 16, value: 99 }
  # }

  # FLEXIBLE_PLAN_MODES = {
  #   "1" => { icon: :public_transport, id: 1 },
  #   "2" => { icon: :bike_sharing, id: 2 },
  #   "3" => { icon: :taxi, id: 3 },
  #   "4" => { icon: :car_sharing, id: 4 }
  # }

  ADDITIONAL_MODE_FEATURES = {
    "suburban_public_transport": [
      { label: "Up to 5 km outside Budapest - addiiton 5,000 Ft/month", id: 1 },
      { label: "Up to 10 km outside Budapest - additional 8,000 Ft/month", id: 2 },
      { label: "Up to 15 km outside Budapest - additional 11,000 Ft/month", id: 3 },
      { label: "Up to 20 km outside Budapest - additional 14,000 Ft/month", id: 4 }
    ],

    "car_rental": [
      { label: "2 full days - additional 7,000 Ft", id: 1 },
      { label: "4 full days - additional 14,000 Ft", id: 2 },
      { label: "6 full days - additional 28,000 Ft", id: 3 },
      { label: "Not every month, but some months I would add on", id: 4 }
    ]
  }

  ATTITUDES_TOWARDS_MAAS = {
    "resident" => [
      [:attitude_towards_maas_1, "attitude_towards_maas_1"],
      [:attitude_towards_maas_2, "attitude_towards_maas_2"],
      [:attitude_towards_maas_3, "attitude_towards_maas_3"],
      [:attitude_towards_maas_4, "attitude_towards_maas_4"],
      [:attitude_towards_maas_5, "attitude_towards_maas_5"],
      [:attitude_towards_maas_6, "attitude_towards_maas_6"],
      [:attitude_towards_maas_7, "attitude_towards_maas_7"],
      [:attitude_towards_maas_8, "attitude_towards_maas_8"],
      [:attitude_towards_maas_9, "attitude_towards_maas_9"]
    ],

    "tourist" => [
      [:attitude_towards_maas_1, "attitude_towards_maas_1"],
      [:attitude_towards_maas_2, "attitude_towards_maas_2"],
      [:attitude_towards_maas_3, "attitude_towards_maas_3"],
      [:attitude_towards_maas_4, "attitude_towards_maas_4"],
      [:attitude_towards_maas_5, "attitude_towards_maas_5"],
      [:attitude_towards_maas_6, "attitude_towards_maas_6"],
      [:attitude_towards_maas_7, "attitude_towards_maas_7"],
      [:attitude_towards_maas_8, "attitude_towards_maas_8"]
    ]
  }

  ATTITUDES_TOWARDS_CAR_OF_OWNERS = {
    "resident" => [
      [:attitude_towards_car_of_owners_1, "attitude_towards_car_of_owners_1"],
      [:attitude_towards_car_of_owners_2, "attitude_towards_car_of_owners_2"],
      [:attitude_towards_car_of_owners_3, "attitude_towards_car_of_owners_3"],
      [:attitude_towards_car_of_owners_4, "attitude_towards_car_of_owners_4"],
      [:attitude_towards_car_of_owners_5, "attitude_towards_car_of_owners_5"],
      [:attitude_towards_car_of_owners_6, "attitude_towards_car_of_owners_6"]
    ],

    "tourist" => [
      [:attitude_towards_car_of_owners_1, "attitude_towards_car_of_owners_1"],
      [:attitude_towards_car_of_owners_2, "attitude_towards_car_of_owners_2"],
      [:attitude_towards_car_of_owners_3, "attitude_towards_car_of_owners_3"],
      [:attitude_towards_car_of_owners_4, "attitude_towards_car_of_owners_4"],
      [:attitude_towards_car_of_owners_5, "attitude_towards_car_of_owners_5"],
      [:attitude_towards_car_of_owners_6, "attitude_towards_car_of_owners_6"]
    ]
  }

  ATTITUDES_TOWARDS_CAR_OF_NON_OWNERS = {
    "resident" => [
      [:attitude_towards_car_of_non_owners_1, "attitude_towards_car_of_non_owners_1"],
      [:attitude_towards_car_of_non_owners_2, "attitude_towards_car_of_non_owners_2"]
    ],

    "tourist" => [
      [:attitude_towards_car_of_non_owners_1, "attitude_towards_car_of_non_owners_1"],
      [:attitude_towards_car_of_non_owners_2, "attitude_towards_car_of_non_owners_2"]
    ]
  }

  MODES_AFFECTED = {
    "resident" => [
      [:modes_affected_option_1, "modes_affected_option_1"],
      [:modes_affected_option_2, "modes_affected_option_2"],
      [:modes_affected_option_3, "modes_affected_option_3"],
      [:modes_affected_option_4, "modes_affected_option_4"],
      [:modes_affected_option_5, "modes_affected_option_5"],
      [:modes_affected_option_6, "modes_affected_option_6"],
      [:modes_affected_option_7, "modes_affected_option_7"],
      [:modes_affected_option_8, "modes_affected_option_8"]
    ],

    "tourist" => [
      [:modes_affected_option_1, "modes_affected_option_1"],
      [:modes_affected_option_2, "modes_affected_option_2"],
      [:modes_affected_option_3, "modes_affected_option_3"],
      [:modes_affected_option_4, "modes_affected_option_4"],
      [:modes_affected_option_5, "modes_affected_option_5"],
      [:modes_affected_option_6, "modes_affected_option_6"],
      [:modes_affected_option_7, "modes_affected_option_7"],
      [:modes_affected_option_8, "modes_affected_option_8"]
    ]
  }

  ## GETTERS ##
  def plans_json
    plans
  end

  # # 1, 2: Design - 1 [non-contextual]
  # # 3, 4, 5: Design - 1 [contextual]
  # # 6: Flexible
  # # 7, 8, 9: Table Design
  # def plans_for_page(page)
  #   case page
  #   when 1..2
  #     plans["design_1"]["non_contextual"][page.to_s]
  #   when 3..5
  #     plans["design_1"]["non_contextual"][page.to_s]
  #   when 7..9
  #     plans["design_4"][]

  # end

  def plans_for_page(page)
    if page == 4
      # Return selected plans
      plans = []
      (1..3).each do |index|
        plans << plans_json[index.to_s][responses["picked_plans"][index.to_s].to_i - 1]
      end

      return plans
    else
      basic_plan = plans_json[page.to_s][0]
      urban_plan = plans_json[page.to_s][1]
      extra_plan = plans_json[page.to_s][2]

      return [basic_plan, urban_plan, extra_plan]
    end
  end

  ### EXPORTER ###
  ## RESIDENT ##

  # INTRO PAGE #
  def pm_down
    responses["download_maas_app"]
  end

  def pm_nodown1
    if ["3", "4", "5"].include?(pm_down)
      responses["reasons_for_not_downloading"].include?("1") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def pm_nodown2
    if ["3", "4", "5"].include?(pm_down)
      responses["reasons_for_not_downloading"].include?("2") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def pm_nodown3
    if ["3", "4", "5"].include?(pm_down)
      responses["reasons_for_not_downloading"].include?("3") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def pm_nodown4
    if ["3", "4", "5"].include?(pm_down)
      responses["reasons_for_not_downloading"].include?("4") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def pm_nodown5
    if ["3", "4", "5"].include?(pm_down)
      responses["reasons_for_not_downloading"].include?("5") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def pfeat_jp_1
    responses["importance_of_features"]["pfeat_jp_1"]
  end

  def pfeat_jp_2
    responses["importance_of_features"]["pfeat_jp_2"]
  end

  def pfeat_jp_3
    responses["importance_of_features"]["pfeat_jp_3"]
  end

  def pfeat_jp_4
    responses["importance_of_features"]["pfeat_jp_5"]
  end

  def pfeat_jp_5
    responses["importance_of_features"]["pfeat_jp_8"]
  end

  def pfeat_jp_6
    responses["importance_of_features"]["pfeat_jp_9"]
  end

  def pfeat_oth_1
    responses["importance_of_features"]["pfeat_oth_1"]
  end

  def pfeat_oth_2
    responses["importance_of_features"]["pfeat_oth_2"]
  end

  def pfeat_oth_3
    responses["importance_of_features"]["pfeat_oth_4"]
  end

  def pfeat_oth_4
    responses["importance_of_features"]["pfeat_oth_5"]
  end

  def pfeat_oth_5
    responses["importance_of_features"]["pfeat_oth_6"]
  end

  def pfeat_oth_6
    responses["importance_of_features"]["pfeat_oth_7"]
  end

  # ATTITUDES PAGE #
  def pm_ex_feel1
    attitudes["maas"]["attitude_towards_maas_1"]
  end

  def pm_ex_feel2
    attitudes["maas"]["attitude_towards_maas_2"]
  end

  def pm_ex_feel3
    attitudes["maas"]["attitude_towards_maas_3"]
  end

  def pm_ex_feel4
    attitudes["maas"]["attitude_towards_maas_4"]
  end

  def pm_ex_feel5
    attitudes["maas"]["attitude_towards_maas_5"]
  end

  def pm_ex_feel6
    attitudes["maas"]["attitude_towards_maas_6"]
  end

  def pm_ex_feel7
    attitudes["maas"]["attitude_towards_maas_7"]
  end

  def pm_ex_feel8
    attitudes["maas"]["attitude_towards_maas_8"]
  end

  def pm_ex_feel9
    attitudes["maas"]["attitude_towards_maas_9"]
  end

  def pm_ex_car1
    attitudes["car_owners"]["attitude_towards_car_of_owners_1"]
  end

  def pm_ex_car2
    attitudes["car_owners"]["attitude_towards_car_of_owners_2"]
  end

  def pm_ex_car3
    attitudes["car_owners"]["attitude_towards_car_of_owners_3"]
  end

  def pm_ex_car4
    attitudes["car_owners"]["attitude_towards_car_of_owners_4"]
  end

  def pm_ex_car5
    attitudes["car_owners"]["attitude_towards_car_of_owners_5"]
  end

  def pm_ex_car6
    attitudes["car_owners"]["attitude_towards_car_of_owners_6"]
  end

  def pm_ex_nocar1
    attitudes["car_non_owners"]["attitude_towards_car_of_non_owners_1"]
  end

  def pm_ex_nocar2
    attitudes["car_non_owners"]["attitude_towards_car_of_non_owners_2"]
  end

  def pm_ex_pt
    attitudes["modes_affected"]["modes_affected_option_1"]
  end

  def pm_ex_rail
    attitudes["modes_affected"]["modes_affected_option_2"]
  end

  def pm_ex_car
    attitudes["modes_affected"]["modes_affected_option_3"]
  end

  def pm_ex_bike
    attitudes["modes_affected"]["modes_affected_option_4"]
  end

  def pm_ex_walk
    attitudes["modes_affected"]["modes_affected_option_5"]
  end

  def pm_ex_taxiapp
    attitudes["modes_affected"]["modes_affected_option_6"]
  end

  def pm_ex_taxi
    attitudes["modes_affected"]["modes_affected_option_7"]
  end

  def pm_ex_rcar
    attitudes["modes_affected"]["modes_affected_option_8"]
  end

  def cc_most
    if responses["company_choice"]
      responses["company_choice"]["most_liked_company"]
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def cc_least
    if responses["company_choice"]
      responses["company_choice"]["least_liked_company"]
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def wave2_part
    if responses["10"]
      responses["10"]["test_maas_app"].to_i
    else
      Exporter::NULL_VALUE_MISSING_VALUE
    end
  end

  def wave2_fg
    if wave2_part.zero? or wave2_part == Exporter::NULL_VALUE_MISSING_VALUE
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    else
      responses["10"]["feedback_maas_app"].to_i
    end
  end

  # PLANS-SPECIFIC #
  # COMPANY 1 #

  # - plan related - #
  def company_1_plan1_bs_cost(task)
    plans["design_4"][task]["payg"]["modes"]["bike_sharing"]["value"]
  end

  def company_1_plan1_tx_cost(task)
    plans["design_4"][task]["payg"]["modes"]["taxi"]["value"]
  end

  def company_1_plan1_cs_cost(task)
    if plans["design_4"][task]["payg"]["modes"]["car_sharing"]
      plans["design_4"][task]["payg"]["modes"]["car_sharing"]["value"]
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_1_plan1_fee(task)
    plans["design_4"][task]["payg"]["elements"]["plan_fee"]["value"]
  end

    def company_1_plan2_pt(task)
      plans["design_4"][task]["weekly"]["modes"]["public_transport"]["value"]
    end

    def company_1_plan2_bs_cost(task)
      plans["design_4"][task]["weekly"]["modes"]["bike_sharing"]["value"]
    end

    def company_1_plan2_tx_cost(task)
      plans["design_4"][task]["weekly"]["modes"]["taxi"]["value"]
    end

    def company_1_plan2_cs_cost(task)
      if plans["design_4"][task]["weekly"]["modes"]["car_sharing"]
        plans["design_4"][task]["weekly"]["modes"]["car_sharing"]["value"]
      else
        Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
      end
    end

    def company_1_plan2_fee_base(task)
      plans["design_4"][task]["weekly"]["modes"]["public_transport"]["price"]
    end

    def company_1_plan2_fee_multiplier(task)
      plans["design_4"][task]["weekly"]["elements"]["plan_fee"]["multiplier"]
    end

    def company_1_plan2_fee(task)
      plans["design_4"][task]["weekly"]["elements"]["plan_fee"]["symbol"]
    end

    def company_1_plan2_contract(task)
      plans["design_4"][task]["weekly"]["elements"]["contract_level"]["value"]
    end

  def company_1_plan3_pt(task)
    plans["design_4"][task]["monthly"]["modes"]["public_transport"]["value"]
  end

  def company_1_plan3_bs_cost(task)
    plans["design_4"][task]["monthly"]["modes"]["bike_sharing"]["value"]
  end

  def company_1_plan3_tx_cost(task)
    plans["design_4"][task]["monthly"]["modes"]["taxi"]["value"]
  end

  def company_1_plan3_cs_cost(task)
    if plans["design_4"][task]["monthly"]["modes"]["car_sharing"]
      plans["design_4"][task]["monthly"]["modes"]["car_sharing"]["value"]
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_1_plan3_fee_base(task)
    plans["design_4"][task]["monthly"]["modes"]["public_transport"]["price"]
  end

  def company_1_plan3_fee_multiplier(task)
    plans["design_4"][task]["monthly"]["elements"]["plan_fee"]["multiplier"]
  end

  def company_1_plan3_fee(task)
    plans["design_4"][task]["monthly"]["elements"]["plan_fee"]["symbol"]
  end

  def company_1_plan3_contract(task)
    plans["design_4"][task]["monthly"]["elements"]["contract_level"]["value"]
  end

  def company_1_chosen_pt(task)
    case company_1_choice(task)
    when "1"
      plans["design_4"][task]["payg"]["modes"]["public_transport"]["value"]
    when "2"
      plans["design_4"][task]["weekly"]["modes"]["public_transport"]["value"]
    when "3"
      plans["design_4"][task]["monthly"]["modes"]["public_transport"]["value"]
    when "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    end
  end

  def company_1_chosen_bs(task)
    case company_1_choice(task)
    when "1"
      plans["design_4"][task]["payg"]["modes"]["bike_sharing"]["value"]
    when "2"
      plans["design_4"][task]["weekly"]["modes"]["bike_sharing"]["value"]
    when "3"
      plans["design_4"][task]["monthly"]["modes"]["bike_sharing"]["value"]
    when "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    end
  end

  def company_1_chosen_tx(task)
    case company_1_choice(task)
    when "1"
      plans["design_4"][task]["payg"]["modes"]["taxi"]["value"]
    when "2"
      plans["design_4"][task]["weekly"]["modes"]["taxi"]["value"]
    when "3"
      plans["design_4"][task]["monthly"]["modes"]["taxi"]["value"]
    when "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    end
  end

  def company_1_chosen_cs(task)
    case company_1_choice(task)
    when "1"
      company_1_plan1_cs_cost(task)
    when "2"
      company_1_plan2_cs_cost(task)
    when "3"
      company_1_plan3_cs_cost(task)
    when "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    end
  end

  def company_1_chosen_fee(task)
    case company_1_choice(task)
    when "1"
      company_1_plan1_fee(task)
    when "2"
      company_1_plan2_fee(task)
    when "3"
      company_1_plan3_fee(task)
    when "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    end
  end

  def company_1_chosen_multiplier(task)
    case company_1_choice(task)
    when "1"
      1 # NOTE: There is no multiplier
    when "2"
      company_1_plan2_fee_multiplier(task)
    when "3"
      company_1_plan3_fee_multiplier(task)
    when "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    end
  end

  # - response related - #
  def company_1_choice(task)
    responses[task]["picked_plan"]
  end

  def company_1_completiontime(task)
    completion_times["page_#{task}"]
  end

  def company_1_section_completiontime(task)
    completion_times["page_1"] + completion_times["page_2"] + completion_times["page_3"]
  end

    # + NOT VALID + #
    def company_1_why_not_1(task)
      "-"
    end

    def company_1_why_not_2(task)
      "-"
    end

    def company_1_why_not_3(task)
      "-"
    end

    def company_1_why_not_4(task)
      "-"
    end

    def company_1_why_not_5(task)
      "-"
    end

    def company_1_why_not_6(task)
      "-"
    end

    def company_1_why_not_7(task)
      "-"
    end

    def company_1_why_not_8(task)
      "-"
    end

    def company_1_why_not_other(task)
      "-"
    end

    def company_1_make_consider(task)
      "-"
    end
    # - NOT VALID - #

  def company_1_plan_pt_use(task)
    if responses[task]["picked_plan"] == "4"
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    else
      responses[task]["number_of_trips_pt"]
    end
  end

  def company_1_plan_bs_use(task)
    if responses[task]["picked_plan"] == "4"
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    else
      responses[task]["number_of_trips_bs"]
    end
  end

  def company_1_plan_tx_use(task)
    if responses[task]["picked_plan"] == "4"
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    else
      responses[task]["number_of_trips_tx"]
    end
  end

  def company_1_plan_cs_use(task)
    if responses[task]["picked_plan"] == "4"
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    else
      responses[task]["number_of_trips_cs"]
    end
  end

  # COMPANY 2 #
  # mode_array = other_data_json["mode_data"]["modes"].collect { |mode| MODE_VARIABLE_MAP.key(mode).to_s }
  # MODE_COMBINATIONS[mode_array.sort]
  MODE_COMBINATIONS = {
    %w(public_transport) => 1,
    %w(bike_sharing) => 2,
    %w(taxi) => 3,
    %w(car_sharing) => 4,
    %w(bike_sharing public_transport) => 5,
    %w(public_transport taxi) => 6,
    %w(car_sharing public_transport) => 7,
    %w(bike_sharing taxi) => 8,
    %w(bike_sharing car_sharing) => 9,
    %w(car_sharing taxi) => 10,
    %w(bike_sharing public_transport taxi) => 11,
    %w(bike_sharing car_sharing public_transport ) => 12,
    %w(car_sharing public_transport taxi) => 13,
    %w(bike_sharing car_sharing taxi) => 14,
    %w(bike_sharing car_sharing public_transport taxi) => 15
  }

  # - plan related - #
  def company_2_context(task)
    case task
    when "1", "2"
      0 # no context
    else
      context_index = (task.to_i - 2).to_s
      case plans["design_1"]["contextual"]["contexts"][context_index]["type"]
      when "carrot"
        1
      when "stick"
        2
      end
    end
  end

  def company_2_carrot(task)
    if company_2_context(task) == 1
      context_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["contexts"][context_index]["deal"]["value"]
    else
      0 # no-context / stick
    end
  end

  def company_2_carrotdur(task)
    if company_2_context(task) == 1
      context_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["contexts"][context_index]["duration"]["value"]
    else
      0 # no-context / stick
    end
  end

  def company_2_stick(task)
    if company_2_context(task) == 2
      context_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["contexts"][context_index]["measure"]["value"]
    else
      0 # no-context / carrot
    end
  end

  def company_2_plan1_pt(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][0]["modes"]["public_transport"]["value"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][0]["modes"]["public_transport"]["value"]
    end
  end

  def company_2_plan1_bs(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][0]["modes"]["bike_sharing"]["value"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][0]["modes"]["bike_sharing"]["value"]
    end
  end

    def company_2_plan1_tx(task)
      if company_2_context(task) == 0
        if plans["design_1"]["non_contextual"][task][0]["modes"]["taxi"]
          plans["design_1"]["non_contextual"][task][0]["modes"]["taxi"]["value"]
        else
          0
        end
      else
        task_index = (task.to_i - 2).to_s
        if plans["design_1"]["contextual"]["plans"][task_index][0]["modes"]["taxi"]
          plans["design_1"]["contextual"]["plans"][task_index][0]["modes"]["taxi"]["value"]
        else
          0
        end
      end
    end

    def company_2_plan1_cs(task)
      if company_2_context(task) == 0
        if plans["design_1"]["non_contextual"][task][0]["modes"]["car_sharing"]
          plans["design_1"]["non_contextual"][task][0]["modes"]["car_sharing"]["value"]
        else
          0
        end
      else
        task_index = (task.to_i - 2).to_s
        if plans["design_1"]["contextual"]["plans"][task_index][0]["modes"]["car_sharing"]
          plans["design_1"]["contextual"]["plans"][task_index][0]["modes"]["car_sharing"]["value"]
        else
          0
        end
      end
    end

  def company_2_plan1_combinations(task)
    if company_2_context(task) == 0
      mode_array = plans["design_1"]["non_contextual"][task][0]["modes"].select { |mode, element| element["value"] != 0 }.keys
    else
      task_index = (task.to_i - 2).to_s
      mode_array = plans["design_1"]["contextual"]["plans"][task_index][0]["modes"].select { |mode, element| element["value"] != 0 }.keys
    end

    MODE_COMBINATIONS[mode_array.sort]
  end

  def company_2_plan1_baseprice(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][0]["base_price"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][0]["base_price"]
    end
  end

  def company_2_plan1_multiplier(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][0]["multiplier"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][0]["multiplier"]
    end
  end

  def company_2_plan1_price(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][0]["final_price"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][0]["final_price"]
    end
  end

    def company_2_plan2_pt(task)
      if company_2_context(task) == 0
        if plans["design_1"]["non_contextual"][task][1]["modes"]["public_transport"]
          plans["design_1"]["non_contextual"][task][1]["modes"]["public_transport"]["value"]
        else
          0
        end
      else
        task_index = (task.to_i - 2).to_s
        if plans["design_1"]["contextual"]["plans"][task_index][1]["modes"]["public_transport"]
          plans["design_1"]["contextual"]["plans"][task_index][1]["modes"]["public_transport"]["value"]
        else
          0
        end
      end
    end

  def company_2_plan2_bs(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][1]["modes"]["bike_sharing"]["value"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][1]["modes"]["bike_sharing"]["value"]
    end
  end

  def company_2_plan2_tx(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][1]["modes"]["taxi"]["value"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][1]["modes"]["taxi"]["value"]
    end
  end

    def company_2_plan2_cs(task)
      if company_2_context(task) == 0
        if plans["design_1"]["non_contextual"][task][1]["modes"]["car_sharing"]
          plans["design_1"]["non_contextual"][task][1]["modes"]["car_sharing"]["value"]
        else
          0
        end
      else
        task_index = (task.to_i - 2).to_s
        if plans["design_1"]["contextual"]["plans"][task_index][1]["modes"]["car_sharing"]
          plans["design_1"]["contextual"]["plans"][task_index][1]["modes"]["car_sharing"]["value"]
        else
          0
        end
      end
    end

  def company_2_plan2_combinations(task)
    if company_2_context(task) == 0
      mode_array = plans["design_1"]["non_contextual"][task][1]["modes"].select { |mode, element| element["value"] != 0 }.keys
    else
      task_index = (task.to_i - 2).to_s
      mode_array = plans["design_1"]["contextual"]["plans"][task_index][1]["modes"].select { |mode, element| element["value"] != 0 }.keys
    end

    MODE_COMBINATIONS[mode_array.sort]
  end

  def company_2_plan2_baseprice(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][1]["base_price"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][1]["base_price"]
    end
  end

  def company_2_plan2_multiplier(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][1]["multiplier"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][1]["multiplier"]
    end
  end

  def company_2_plan2_price(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][1]["final_price"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][1]["final_price"]
    end
  end

    def company_2_plan3_pt(task)
      if company_2_context(task) == 0
        if plans["design_1"]["non_contextual"][task][2]["modes"]["public_transport"]
          plans["design_1"]["non_contextual"][task][2]["modes"]["public_transport"]["value"]
        else
          0
        end
      else
        task_index = (task.to_i - 2).to_s
        if plans["design_1"]["contextual"]["plans"][task_index][2]["modes"]["public_transport"]
          plans["design_1"]["contextual"]["plans"][task_index][2]["modes"]["public_transport"]["value"]
        else
          0
        end
      end
    end

    def company_2_plan3_bs(task)
      if company_2_context(task) == 0
        if plans["design_1"]["non_contextual"][task][2]["modes"]["bike_sharing"]
          plans["design_1"]["non_contextual"][task][2]["modes"]["bike_sharing"]["value"]
        else
          0
        end
      else
        task_index = (task.to_i - 2).to_s
        if plans["design_1"]["contextual"]["plans"][task_index][2]["modes"]["bike_sharing"]
          plans["design_1"]["contextual"]["plans"][task_index][2]["modes"]["bike_sharing"]["value"]
        else
          0
        end
      end
    end

  def company_2_plan3_tx(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][2]["modes"]["taxi"]["value"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][2]["modes"]["taxi"]["value"]
    end
  end

  def company_2_plan3_cs(task)
    if company_2_context(task) == 0
      if plans["design_1"]["non_contextual"][task][2]["modes"]["car_sharing"]
        plans["design_1"]["non_contextual"][task][2]["modes"]["car_sharing"]["value"]
      else
        0
      end
    else
      task_index = (task.to_i - 2).to_s
      if plans["design_1"]["contextual"]["plans"][task_index][2]["modes"]["car_sharing"]
        plans["design_1"]["contextual"]["plans"][task_index][2]["modes"]["car_sharing"]["value"]
      else
        0
      end
    end
  end

  def company_2_plan3_combinations(task)
    if company_2_context(task) == 0
      mode_array = plans["design_1"]["non_contextual"][task][2]["modes"].select { |mode, element| element["value"] != 0 }.keys
    else
      task_index = (task.to_i - 2).to_s
      mode_array = plans["design_1"]["contextual"]["plans"][task_index][2]["modes"].select { |mode, element| element["value"] != 0 }.keys
    end

    MODE_COMBINATIONS[mode_array.sort]
  end

  def company_2_plan3_baseprice(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][2]["base_price"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][2]["base_price"]
    end
  end

  def company_2_plan3_multiplier(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][2]["multiplier"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][2]["multiplier"]
    end
  end

  def company_2_plan3_price(task)
    if company_2_context(task) == 0
      plans["design_1"]["non_contextual"][task][2]["final_price"]
    else
      task_index = (task.to_i - 2).to_s
      plans["design_1"]["contextual"]["plans"][task_index][2]["final_price"]
    end
  end

  # - response related - #
  def company_2_choice(task)
    task_index = (task.to_i + 3).to_s
    responses[task_index]["picked_plan"]
  end

  def company_2_completiontime(task)
    task_index = (task.to_i + 3).to_s
    completion_times["page_#{task_index}"]
  end

  def company_2_section_completiontime(task)
    completion_times["page_4"] + completion_times["page_5"] + completion_times["page_6"] +
      completion_times["page_7"] + completion_times["page_8"]
  end

  def company_2_chosen_pt(task)
    choice = company_2_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("company_2_plan#{choice}_pt", task)
      # task_index = (task.to_i - 2).to_s
      # choice_index = choice.to_i - 1
      # plans["design_1"]["contextual"]["plans"][task_index][choice_index]["modes"]["public_transport"]["value"]
    end

    # company_2_plan1_pt
  end

  def company_2_chosen_bs(task)
    choice = company_2_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("company_2_plan#{choice}_bs", task)
      # task_index = (task.to_i - 2).to_s
      # choice_index = choice.to_i - 1
      # plans["design_1"]["contextual"]["plans"][task_index][choice_index]["modes"]["bike_sharing"]["value"]
    end
  end

  def company_2_chosen_tx(task)
    choice = company_2_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("company_2_plan#{choice}_tx", task)
      # task_index = (task.to_i - 2).to_s
      # choice_index = choice.to_i - 1
      # if plans["design_1"]["contextual"]["plans"][task_index][choice_index]["modes"]["taxi"]
      #   plans["design_1"]["contextual"]["plans"][task_index][choice_index]["modes"]["taxi"]["value"]
      # else
      #   0
      # end
    end
  end

  def company_2_chosen_cs(task)
    choice = company_2_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("company_2_plan#{choice}_cs", task)
      # task_index = (task.to_i - 2).to_s
      # choice_index = choice.to_i - 1
      # if plans["design_1"]["contextual"]["plans"][task_index][choice_index]["modes"]["car_sharing"]
      #   plans["design_1"]["contextual"]["plans"][task_index][choice_index]["modes"]["car_sharing"]["value"]
      # else
      #   0
      # end
    end
  end

  def company_2_chosen_combination(task)
    choice = company_2_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("company_2_plan#{choice}_combinations", task)
      # task_index = (task.to_i - 2).to_s
      # choice_index = choice.to_i - 1
      # mode_array = plans["design_1"]["contextual"]["plans"][task_index][choice_index]["modes"].select { |mode, element| element["value"] != 0 }.keys
      # MODE_COMBINATIONS[mode_array.sort]
    end
  end

  def company_2_chosen_price(task)
    choice = company_2_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("company_2_plan#{choice}_price", task)
      # task_index = (task.to_i - 2).to_s
      # choice_index = choice.to_i - 1
      # plans["design_1"]["contextual"]["plans"][task_index][choice_index]["base_price"]
    end
  end

  def company_2_chosen_multiplier(task)
    choice = company_2_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("company_2_plan#{choice}_multiplier", task)
      # task_index = (task.to_i - 2).to_s
      # choice_index = choice.to_i - 1
      # plans["design_1"]["contextual"]["plans"][task_index][choice_index]["multiplier"]
    end
  end

  def company_2_why_not_1(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("1") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_2(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("2") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_3(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("3") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_4(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("4") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_5(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("5") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_6(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("6") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_7(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("7") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_8(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["reasons_for_not_choosing"].include?("8") ? 1 : 0
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_why_not_other(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["other_reason"].blank? ? -3 : responses[task_index]["other_reason"]
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def company_2_make_consider(task)
    if company_2_choice(task) == "4"
      task_index = (task.to_i + 3).to_s
      responses[task_index]["will_consider_buying_if"].blank? ? -3 : responses[task_index]["will_consider_buying_if"]
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  # COMPANY 3 #
  def company_3_choice_combination
    mode_array = flexible_plan["plan"]["modes"].select { |mode, value| value != "0" }.keys
    MODE_COMBINATIONS[mode_array.sort]
  end

  def company_3_pt
    flexible_plan["plan"]["modes"]["public_transport"] || 0
  end

  def company_3_bs
    flexible_plan["plan"]["modes"]["bike_sharing"] || 0
  end

  def company_3_tx
    flexible_plan["plan"]["modes"]["taxi"] || 0
  end

  def company_3_cs
    flexible_plan["plan"]["modes"]["car_sharing"] || 0
  end

  def company_3_baseprice
    flexible_plan["plan"]["price"]
  end

  def company_3_multiplier
    "-"
  end

  def company_3_price
    flexible_plan["plan"]["price"]
  end

  def company_3_buy
    flexible_plan["responses"]["buy_chosen_plan"]
  end

  def company_3_nobuy_price
    flexible_plan["responses"]["proposed_amount"]
  end

  def company_3_completiontime
    completion_times["flexible_plan"]
  end

  ## TOURIST ##
  def tmaasdownload
    responses["download_maas_app"]
  end

  def tnomaasdownload
    if ["3", "4"].include?(tmaasdownload)
      responses["reasons_for_not_downloading"]
    else
      Exporter::NULL_VALUE_QUESTION_NOT_SHOWN
    end
  end

  def tmaasfeature1_most
    responses["most_imporant_features"].include?("1") ? 1 : 0
  end

  def tmaasfeature1_least
    responses["least_imporant_features"].include?("1") ? 1 : 0
  end

  def tmaasfeature2_most
    responses["most_imporant_features"].include?("2") ? 1 : 0
  end

  def tmaasfeature2_least
    responses["least_imporant_features"].include?("2") ? 1 : 0
  end

  def tmaasfeature3_most
    responses["most_imporant_features"].include?("3") ? 1 : 0
  end

  def tmaasfeature3_least
    responses["least_imporant_features"].include?("3") ? 1 : 0
  end

  def tmaasfeature4_most
    responses["most_imporant_features"].include?("4") ? 1 : 0
  end

  def tmaasfeature4_least
    responses["least_imporant_features"].include?("4") ? 1 : 0
  end

  def tmaasfeature5_most
    responses["most_imporant_features"].include?("5") ? 1 : 0
  end

  def tmaasfeature5_least
    responses["least_imporant_features"].include?("5") ? 1 : 0
  end

  def tmaasfeature6_most
    responses["most_imporant_features"].include?("6") ? 1 : 0
  end

  def tmaasfeature6_least
    responses["least_imporant_features"].include?("6") ? 1 : 0
  end

  def tmaasfeature7_most
    responses["most_imporant_features"].include?("7") ? 1 : 0
  end

  def tmaasfeature7_least
    responses["least_imporant_features"].include?("7") ? 1 : 0
  end

  def tmaasfeature8_most
    responses["most_imporant_features"].include?("8") ? 1 : 0
  end

  def tmaasfeature8_least
    responses["least_imporant_features"].include?("8") ? 1 : 0
  end

  def tmaasplanatt1
    attitudes["maas"]["attitude_towards_maas_1"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  def tmaasplanatt2
    attitudes["maas"]["attitude_towards_maas_2"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  def tmaasplanatt3
    attitudes["maas"]["attitude_towards_maas_3"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  def tmaasplanatt4
    attitudes["maas"]["attitude_towards_maas_4"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  def tmaasplanatt5
    attitudes["maas"]["attitude_towards_maas_5"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  def tmaasplanatt6
    attitudes["maas"]["attitude_towards_maas_6"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  def tmaasplanatt7
    attitudes["maas"]["attitude_towards_maas_7"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  def tmaasplanatt8
    attitudes["maas"]["attitude_towards_maas_8"] || Exporter::NULL_VALUE_MISSING_VALUE
  end

  # PLANS #
  def tourist_duration
    duration = user.pre_survey.ttravelduration.to_i

    # 1 -> 24h
    if duration == 1
      "1"
    # 2-4 -> 72h
    elsif duration <= 4
      "2"
    # >4 -> 1w
    else
      "3"
    end
  end

  def tourist_choice(task)
    responses[task]["picked_plan"]
  end

  def tourist_plan1_pt(task)
    plans[task][0]["modes"]["public_transport"]["value"]
  end

  def tourist_plan1_bs(task)
    plans[task][0]["modes"]["bike_sharing"]["value"]
  end

  def tourist_plan1_card(task)
    card_attribute = (user.city_id == City::OXFORDSHIRE) ? "manchester_card" : "budapest_card"
    plans[task][0]["modes"][card_attribute]["value"]
  end

  def tourist_plan1_combinations(task)
    mode_array = plans[task][0]["modes"].select { |mode, element| !["manchester_card", "budapest_card"].include?(mode) and element["value"] != 0 }.keys
    MODE_COMBINATIONS[mode_array.sort]
  end

  def tourist_plan1_baseprice(task)
    plans[task][0]["base_price"]
  end

  def tourist_plan1_multiplier(task)
    plans[task][0]["multiplier"]
  end

  def tourist_plan1_price(task)
    plans[task][0]["final_price"]
  end

  def tourist_plan2_bs(task)
    plans[task][1]["modes"]["bike_sharing"]["value"]
  end

  def tourist_plan2_tx(task)
    plans[task][1]["modes"]["taxi"]["value"]
  end

  # -1= if hotel presented; 0= not shown; 1=shared shuttle bus; 2= taxi both ways
  def tourist_plan2_airport(task)
    if plans[task][1]["modes"]["airport_transfer"]
      plans[task][1]["modes"]["airport_transfer"]["value"]
    elsif plans[task][1]["modes"]["hotel_transfer"]
      -1
    else
      0
    end
  end

  # -1= if airport presented; 0= not shown; 1=shared shuttle bus; 2= taxi both ways
  def tourist_plan2_hotel(task)
    if plans[task][1]["modes"]["hotel_transfer"]
      plans[task][1]["modes"]["hotel_transfer"]["value"]
    elsif plans[task][1]["modes"]["airport_transfer"]
      -1
    else
      0
    end
  end

  def tourist_plan2_card(task)
    card_attribute = (user.city_id == City::OXFORDSHIRE) ? "manchester_card" : "budapest_card"
    plans[task][1]["modes"][card_attribute]["value"]
  end

  def tourist_plan2_combinations(task)
    mode_array = plans[task][1]["modes"].select { |mode, element|
      !["manchester_card", "budapest_card", "airport_transfer", "hotel_transfer"].include?(mode) and element["value"] != 0
    }.keys
    MODE_COMBINATIONS[mode_array.sort]
  end

  def tourist_plan2_baseprice(task)
    plans[task][1]["base_price"]
  end

  def tourist_plan2_multiplier(task)
    plans[task][1]["multiplier"]
  end

  def tourist_plan2_price(task)
    plans[task][1]["final_price"]
  end

  def tourist_plan3_tx(task)
    plans[task][2]["modes"]["taxi"]["value"]
  end

  def tourist_plan3_cs(task)
    if plans[task][2]["modes"]["car_sharing"]
      plans[task][2]["modes"]["car_sharing"]["value"]
    else
      0
    end
  end

  # -1= if hotel presented; 0= not shown; 1=shared shuttle bus; 2= taxi both ways
  def tourist_plan3_airport(task)
    if plans[task][2]["modes"]["airport_transfer"]
      plans[task][2]["modes"]["airport_transfer"]["value"]
    elsif plans[task][2]["modes"]["hotel_transfer"]
      -1
    else
      0
    end
  end

  # -1= if airport presented; 0= not shown; 1=shared shuttle bus; 2= taxi both ways
  def tourist_plan3_hotel(task)
    if plans[task][2]["modes"]["hotel_transfer"]
      plans[task][2]["modes"]["hotel_transfer"]["value"]
    elsif plans[task][2]["modes"]["airport_transfer"]
      -1
    else
      0
    end
  end

  def tourist_plan3_card(task)
    card_attribute = (user.city_id == City::OXFORDSHIRE) ? "manchester_card" : "budapest_card"
    plans[task][2]["modes"][card_attribute]["value"]
  end

  def tourist_plan3_combinations(task)
    mode_array = plans[task][2]["modes"].select { |mode, element|
      !["manchester_card", "budapest_card", "airport_transfer", "hotel_transfer"].include?(mode) and element["value"] != 0
    }.keys
    MODE_COMBINATIONS[mode_array.sort]
  end

  def tourist_plan3_baseprice(task)
    plans[task][2]["base_price"]
  end

  def tourist_plan3_multiplier(task)
    plans[task][2]["multiplier"]
  end

  def tourist_plan3_price(task)
    plans[task][2]["final_price"]
  end

  def tourist_completiontime(task)
    completion_times["page_#{task}"]
  end

  def tourist_section_completiontime(task)
    completion_times["page_1"] + completion_times["page_2"] + completion_times["page_3"]
  end

  def tourist_chosen_pt(task)
    choice = tourist_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      begin
        send("tourist_plan#{choice}_pt", task)
      rescue
        1
      end
    end
  end

  def tourist_chosen_bs(task)
    choice = tourist_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      begin
        send("tourist_plan#{choice}_bs", task)
      rescue
        1
      end
    end
  end

  def tourist_chosen_tx(task)
    choice = tourist_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      begin
        send("tourist_plan#{choice}_tx", task)
      rescue
        -1
      end
    end
  end

  def tourist_chosen_cs(task)
    choice = tourist_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      begin
        send("tourist_plan#{choice}_cs", task)
      rescue
        -1
      end
    end
  end

  def tourist_chosen_combination(task)
    choice = tourist_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("tourist_plan#{choice}_combinations", task)
    end
  end

  def tourist_chosen_price(task)
    choice = tourist_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("tourist_plan#{choice}_price", task)
    end
  end

  def tourist_chosen_multiplier(task)
    choice = tourist_choice(task)
    if choice == "4"
      Exporter::NULL_VALUE_MISSING_VALUE
    else
      send("tourist_plan#{choice}_multiplier", task)
    end
  end

end